

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2> Get data from HTML form and take it by PHP</h2>
    </header>

    <div class="maincontent">
<?php

if (isset($_POST['submit'])){


    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    $language = $_POST['language'];


?>

        <table>
            <tr>
                <td  colspan="2" >Output</td>
            </tr>

            <tr>
                <td>Name:</td><td><?php echo $name; ?></span></td>
            </tr>
            <tr>
                <td>Gender:</td>

                <?php
                if($gender =="Male"){?>
                    <td> <?php echo "Male"?></td>
                <?php } elseif ( $gender == "Female"){?>

                    <td> <?php  echo "Female";?></td>

                <?php }?>
            </tr>

            <tr>
                <td>Department:</td>
                <?php
                if($department =="CSE"){?>
                    <td> <?php echo "CSE"?></td>
                <?php } elseif ( $department == "EEE"){?>

                    <td> <?php  echo "EEE";?></td>

                <?php }elseif ( $department == "ETE"){?>
                    <td> <?php  echo "ETE";?></td>
                <?php } ?>
            </tr>

            <tr>
                <td>Language:</td><td> <?php  echo $language;?></td>
            </tr>

        </table>
<?php } ?>

        <hr>

        <form action="get_allData_by_php.php"  method="post" name="myform" id="form"  onsubmit="selectValue(); return false;">
            <table>
                <tr>
                    <td>Name:</td>
                    <td>
                        <input type="text" name="name" required/>
                    </td>
                </tr>

                <tr>
                    <td>Gender:</td>
                    <td>
                        <input type="radio" name="gender" value="Male"/>Male
                        <input type="radio" name="gender" value="Female"/>Female
                    </td>
                </tr>

                <tr>
                    <td>Department:</td>
                    <td>
                        <input type="checkbox" name="department"  value="CSE"/> CSE
                        <input type="checkbox" name="department" value="EEE"/> EEE
                        <input type="checkbox" name="department" value="ETE"/> ETE
                    </td>
                </tr>

                <tr>
                    <td>Language:</td>
                    <td>
                        <select name="language" >
                            <option >Select One</option>
                            <option value="HTML">HTML</option>
                            <option value="CSS">CSS</option>
                            <option value="JS">JS</option>
                            <option value="PHP">PHP</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" value="submit"/>
                        <input type="reset" value="clear"/>
                    </td>
                </tr>
            </table>
        </form>



    </div>

    <footer class="footer">
        <h2>Hi!! welcome to PHP  practise</h2>
    </footer>
</section>

</body>
</html>