

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Get data from HTML form and take it by PHP</h2>
    </header>

    <div class="maincontent">

<?php


if(isset($_POST['submit'])){

    $value = $_POST['coder'];
    echo "You are " . $value ." coder.";
}

?>

        <form name="myform" id="form"  action="get_val_from_selection.php" method="post">
            <table>
                <tr>
                    <td>Language:</td>
                    <td>
                        <select name="coder" id="">

                            <option >Select One</option>
                            <option value="HTML">HTML</option>
                            <option value="CSS">CSS</option>
                            <option value="JS">JS</option>
                            <option value="PHP">PHP</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" value="submit"/>
                        <input type="reset" value="clear"/>
                    </td>
                </tr>
            </table>
        </form>



    </div>

    <footer class="footer">
        <h2>Hi!! welcome to PHP  practise</h2>
    </footer>
</section>

</body>
</html>