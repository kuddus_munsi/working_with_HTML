

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Get data from HTML form and take it by PHP</h2>
    </header>

    <div class="maincontent">

        <?php

        if (isset($_POST['coder'])){

            $lang = $_POST['coder'];
            echo "your language is:" ;
            foreach ($lang as $keys=>$value){

                echo $value ." ,";
            }
        }

        ?>

        <form action="get_val_from_ckbox.php" method="post" name="myform" id="form"   >
            <table>
                <tr>
                    <td>Language:</td>
                    <td><input type="checkbox" name="coder[]" value="PHP" />PHP
                        <input type="checkbox" name="coder[]" value="JavaScript" />JavaScript
                        <input type="checkbox" name="coder[]" value="HTML" />HTML
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" value="submit"/>
                        <input type="reset" value="clear"/>
                    </td>
                </tr>
            </table>
        </form>



    </div>

    <footer class="footer">
        <h2>Hi!! welcome to PHP  practise</h2>
    </footer>
</section>

</body>
</html>