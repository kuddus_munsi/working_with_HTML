

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Get data from HTML form and take it by PHP</h2>
    </header>

    <div class="maincontent">

        <script>
            function formFunction(){


                var name = document.myform.username.value;
                var shoedata = "The username is:" + name;
                document.getElementById("output").innerHTML= shoedata;
            }
        </script>

        <div id = "output"></div>

        <form name="myform" id="form"  onsubmit="formFunction(); return false;" >
            <table>
                <tr>
                    <td>UserName:</td>
                    <td><input type="text" name="username" required/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="submit"/>
                        <input type="reset" value="clear"/></td>
                </tr>
            </table>
        </form>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to PHP  practise</h2>
    </footer>
</section>

</body>
</html>