

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Get data from HTML form and take it by PHP</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2>Php array_column function practise</h2>
    </header>

    <div class="maincontent">


        <form  action="get_radio_val.php" method="post" name="myform" id="form"    >
            <table>
                <tr>
                    <td>Gender:</td>
                    <td><input type="radio" name="gender" value="Male" required/>Male
                    <input type="radio" name="gender" value="Female" required/>Female
                    <input type="radio" name="gender" value="others" required/>others
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="submit"/>
                        <input type="reset" value="clear"/></td>
                </tr>
            </table>
        </form>

        <?php

        if(isset($_POST['gender']))
        {
            $gender= $_POST['gender'];
             echo "You are "  . $gender;
        }

        ?>


    </div>

    <footer class="footer">
        <h2>Hi!! welcome to PHP  practise</h2>
    </footer>
</section>

</body>
</html>