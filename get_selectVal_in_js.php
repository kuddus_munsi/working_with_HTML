

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2> Get data from HTML form and take it by PHP</h2>
    </header>

    <div class="maincontent">

        <script>

            function selectValue(){
            var index = document.myform.coder.selectedIndex;
            var showData = document.myform.coder.options[index].value;

            var result = "you are " + showData + " coder";
            document.getElementById("value").innerHTML = result;
        }

        </script>

<div id="value">

</div>

        <form name="myform" id="form"  onsubmit="selectValue(); return false;">
            <table>
                <tr>
                    <td>Language:</td>
                    <td>
                        <select name="coder" id="">

                            <option >Select One</option>
                            <option value="HTML">HTML</option>
                            <option value="CSS">CSS</option>
                            <option value="JS">JS</option>
                            <option value="PHP">PHP</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" value="submit"/>
                        <input type="reset" value="clear"/>
                    </td>
                </tr>
            </table>
        </form>



    </div>

    <footer class="footer">
        <h2>Hi!! welcome to PHP  practise</h2>
    </footer>
</section>

</body>
</html>