

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php array function practise</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="content">


    <header class="header">
        <h2> Get data from HTML form and take it by PHP</h2>
    </header>

    <div class="maincontent">

        <script>
            function  selectValue(){
                var name = document.myform.name.value;
                document.getElementById("ShowName").innerHTML = name;


                var genderLength = document.myform.gender.length;
                for(i=0;i<genderLength;i++){
                    var checkGender = document.myform.gender[i].checked;
                    if(checkGender){
                        var genderValue = document.myform.gender[i].value;
                    }
                }
                document.getElementById("ShowGender").innerHTML = genderValue;
                var departmentLength = document.myform.department.length;
                for(i=0;i<departmentLength;i++){
                    var checkDepartment = document.myform.department[i].checked;
                    if(checkDepartment){
                        var departmentValue = document.myform.department[i].value;
                    }
                }
                document.getElementById("ShowDepartment").innerHTML = departmentValue;
                var languageIndex = document.myform.language.selectedIndex;
                var languageVlaue = document.myform.language.options[languageIndex].value;
                document.getElementById("Showlanguage").innerHTML = languageVlaue;


            }
        </script>

        <table>
            <tr>
            <td  colspan="2" >Output</td>
            </tr>

            <tr>
                <td>Name:</td><td><span id ="ShowName"> </span></td>
            </tr>

            <tr>
                <td>Gender:</td><td><span id ="ShowGender"> </span></td>
            </tr>

            <tr>
                <td>Department:</td><td><span id ="ShowDepartment"> </span></td>
            </tr>

            <tr>
                <td>Language:</td><td><span id ="Showlanguage"> </span></td>
            </tr>
        </table>


        <hr>

        <form name="myform" id="form"  onsubmit="selectValue(); return false;">
            <table>
                <tr>
                    <td>Name:</td>
                    <td>
                        <input type="text" name="name" required/>
                    </td>
                </tr>

                <tr>
                    <td>Gender:</td>
                    <td>
                        <input type="radio" name="gender" value="Male"/>Male
                        <input type="radio" name="gender" value="Female"/>Female
                    </td>
                </tr>

                <tr>
                    <td>Department:</td>
                    <td>
                        <input type="checkbox" name="department"  value="CSE"/> CSE
                        <input type="checkbox" name="department" value="EEE"/> EEE
                        <input type="checkbox" name="department" value="ETE"/> ETE
                    </td>
                </tr>

                <tr>
                    <td>Language:</td>
                    <td>
                        <select name="language" >
                            <option >Select One</option>
                            <option value="HTML">HTML</option>
                            <option value="CSS">CSS</option>
                            <option value="JS">JS</option>
                            <option value="PHP">PHP</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" value="submit"/>
                        <input type="reset" value="clear"/>
                    </td>
                </tr>
            </table>
        </form>



    </div>

    <footer class="footer">
        <h2>Hi!! welcome to PHP  practise</h2>
    </footer>
</section>

</body>
</html>